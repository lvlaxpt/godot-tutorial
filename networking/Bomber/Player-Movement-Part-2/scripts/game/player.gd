extends KinematicBody2D

const MOTION_SPEED = 90.0

#w - what is the diff use of puppet_pos n motion?
#w - why must use keyword puppet?
puppet var puppet_pos = Vector2()
puppet var puppet_motion = Vector2()


func _physics_process(_delta):
	var motion = Vector2()

	# is_network_master() means if the node whether client or
	# server is making the movement, if client move it will update
	# it movement and at same time will inform server to update
	# player movement via rset(), server will do the same to client 
	# if it moves.
	
	# position is a member variable of kinematics2D body
	# it provides current location of kinematic2dBody
	# @see https://docs.godotengine.org/en/latest/classes/class_node2d.html#member-variables
	#print("is network master ", is_network_master())
	if is_network_master():
		print("player name ", GameState.player_name)
		if Input.is_action_pressed("ui_left"):
			motion += Vector2(-1, 0)
		if Input.is_action_pressed("ui_right"):
			motion += Vector2(1, 0)
		if Input.is_action_pressed("ui_up"):
			motion += Vector2(0, -1)
		if Input.is_action_pressed("ui_down"):
			motion += Vector2(0, 1)

		rset("puppet_motion", motion)
		rset("puppet_pos", position)
	else:
		position = puppet_pos
		motion = puppet_motion
		#pass

	# FIXME: Use move_and_slide
	move_and_slide(motion * MOTION_SPEED)
	if not is_network_master():
		puppet_pos = position # To avoid jitter

func set_player_name(new_name):
	get_node("label").set_text(new_name)


func _ready():
	puppet_pos = position
