extends Node

# Default game server port. Can be any number between 1024 and 49151.
# Not on the list of registered or common ports as of November 2020:
# https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
const DEFAULT_PORT = 10567

# Max number of players.
const MAX_PEERS = 12

# Name for my player.
var player_name = "The Warrior"
var peer = null

# Names for remote players in id:name format.
var players = {}

# Signals to let lobby GUI know what's going on.
signal connection_succeeded()
signal player_list_changed()

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("connected_to_server", self, "_connected_ok")


# Callback from SceneTree, only for clients (not server).
func _connected_ok():
	# We just connected to a server
	print("game state - connected_ok()", " time stamp: ", OS.get_system_time_msecs())
	emit_signal("connection_succeeded")

# Callback from SceneTree.
func _player_connected(id):
	# Registration of a client beings here, tell the connected player that we are here.
	print("game state - player connected(). ID: ", id, " time stamp: ", OS.get_system_time_msecs())
	rpc_id(id, "register_player", player_name)


# Lobby management functions.
remote func register_player(new_player_name):
	var id = get_tree().get_rpc_sender_id()
	print("game state - register player() ", id, " time stamp: ", OS.get_system_time_msecs())
	players[id] = new_player_name
	emit_signal("player_list_changed")


func host_game(new_player_name):
	player_name = new_player_name
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(peer)


func join_game(ip, new_player_name):
	player_name = new_player_name
	peer = NetworkedMultiplayerENet.new()
	peer.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(peer)


func get_player_list():
	return players.values()


func get_player_name():
	return player_name
