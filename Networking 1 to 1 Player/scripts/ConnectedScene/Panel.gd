extends Panel

onready var nameLabel = $NameLabel
onready var ipLabel = $IPLabel
onready var isServerLabel = $IsSeverLabel
onready var statusLabel = $StatusLabel
onready var button = $Button
onready var textEdit = $TextEdit
onready var messageLabel = $MessageLabel

# linux port must be above 1024 to avoid any permission issue
const SERVER_PORT = 1555


var connectedPlayers: int = 0

var playersInfo = {}
var connectedPlayerID: int = 0

# https://gamefromscratch.com/godot-3-tutorial-networking/ 
# ref 

# Called when the node enters the scene tree for the first time.
func _ready():
	var peer = NetworkedMultiplayerENet.new()

	ipLabel.text = ipLabel.text + Global.ipAddress
	button.connect("pressed", self, "sendMessage")
	
	if(Global.isServer):
		peer.create_server(SERVER_PORT, 1)
		isServerLabel.text = "Server"
		nameLabel.text = nameLabel.text + Global.serverPlayerName
		playersInfo[1] = Global.serverPlayerName
		
	else:
		peer.create_client(Global.ipAddress, SERVER_PORT)
		isServerLabel.text = "Client"
		nameLabel.text = nameLabel.text + Global.clientPlayerName


	get_tree().network_peer = peer
	

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

	
func _player_connected(ID: int):
	statusLabel.text = "Player ID connected: " + str(ID)

	rpc_id(ID, "register_player", playersInfo)


remote func register_player(info):
	# Get the id of the RPC sender.
	if(connectedPlayers != 0):
		print("Player already connected.")
		return false
	
	connectedPlayers = connectedPlayers + 1
	var ID = get_tree().get_rpc_sender_id()
	# Store the info
	connectedPlayerID = ID
	playersInfo[connectedPlayerID] = Global.clientPlayerName
	return true
	
func sendMessage():
	if(connectedPlayerID == 0):
		print("No player connected")
		return false
		
	var ID = get_tree().get_rpc_sender_id()
	
	#if server, it will return ID = 0, so has to set
	# manually to 1
	if(ID == 0):
		ID = 1

	var message = messageLabel.text + "\n"  + str(playersInfo[ID]) + ": " +  textEdit.text
	textEdit.text = ""
	
	rpc_id(connectedPlayerID, "updateMessage", message)
	return true
	
remote func updateMessage(message):

	messageLabel.text = message
	return true

	
