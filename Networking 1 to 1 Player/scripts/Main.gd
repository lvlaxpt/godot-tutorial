extends Node


onready var btn = $Button
onready var checkBtn = $CheckButton
# onready var textEdit = $TextEdit
onready var ipCtrl = $IPControl
onready var nameCtrl = $NameControl

var isServer: bool = false
var connectedScene: String = "res://Connected.tscn"
#const SERVER_PORT = 130
#const MAX_PLAYERS = 2


# Called when the node enters the scene tree for the first time.
func _ready():
	# pass # Replace with function body.
	#var peer = NetworkedMultiplayerENet.new()
	#peer.create_server(SERVER_PORT, MAX_PLAYERS)
	#get_tree().network_peer = peer
	
	#print("Is server: ", get_tree().is_network_server())
	
	btn.connect("button_pressed", self, "submit")
	checkBtn.connect("sendIsServer", self, "setServerClient")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func setServerClient(server: bool):
	self.isServer = server
	if(isServer):
		ipCtrl.hide()
	else:
		ipCtrl.show()
	
func getIPAddress():
	#print("IPs: ", IP.get_local_addresses())	
	for ip in IP.get_local_addresses():
		if "." in ip:
			if !"127" in ip:			
				return ip
				
	return null
	
func submit():
	print("is Server: ", isServer)
	print("IP address: ", getIPAddress())
	print("name ", nameCtrl.getInput())


	Global.isServer = isServer
	if(!isServer):
		print("server ip key in: ", ipCtrl.getInput())
		Global.ipAddress = ipCtrl.getInput()
		Global.clientPlayerName = nameCtrl.getInput()
	else:
		Global.serverPlayerName = nameCtrl.getInput()
		Global.ipAddress = getIPAddress()
		
	get_tree().change_scene(connectedScene)
			
