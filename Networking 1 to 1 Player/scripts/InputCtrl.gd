extends Control

onready var textEdit = $TextEdit
onready var label = $Label

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func show():
	textEdit.show()
	label.show()
	
func hide():
	textEdit.hide()
	label.hide()
	
	
func getInput():
	#pass
	return textEdit.get_text()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
