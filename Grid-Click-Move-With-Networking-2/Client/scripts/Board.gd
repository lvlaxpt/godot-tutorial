extends Control

# $ is shorthand for get_node(path_to_node)
# @see https://godotengine.org/qa/67936/what-does-%24-sign-mean
onready var statusBarControl = $StatusBarControl
onready var boardControl = $BoardControl
onready var yourLabel = $YourLabel
onready var otherPlayerLabel = $OtherPlayerLabel

# linux port must be above 1024 to avoid any permission issue
const SERVER_PORT = 1555

var isSelected = false
var occupiedX = 0
var occupiedY = 0
var board = null
var connectedPlayers: int = 0


# Called when the node enters the scene tree for the first time.

# <source>.connect(signal, target, target_function)
# if custom_pressed signal is emitted (by clicking a button)
# Control @self onTextureButtonPressed is called and button 
# is passed as parameter
func _ready():	

	print("change screen")
	#var peer = NetworkedMultiplayerENet.new()
	#var serverIP = GLOBAL.IPAddressServer
	
	#peer.create_client(serverIP, SERVER_PORT)
	#get_tree().network_peer = peer
	
	#var info = {}
	#var ID = get_tree().get_network_unique_id()
	#info[ID] = {"name": GLOBAL.playerName, "pieceType": "DINOSAUR", "ID": ID}
	#print("connect to server info ", info)


	board = boardControl.getBoard()
	#spotX0Y0.setPiece()
	var rowCount = 0
	var colCount = 0
	
	for row in board:
		colCount = 0
		for btn in row:		
			btn.setXY(rowCount, colCount)
			colCount = colCount + 1
			btn.connect( "custom_pressed", self, "onTextureButtonPressed" )
		
		rowCount = rowCount + 1
		
	var peer = NetworkedMultiplayerENet.new()
	var serverIP = GLOBAL.IPAddressServer
	
	peer.create_client(serverIP, SERVER_PORT)
	get_tree().network_peer = peer
	var ID = get_tree().get_network_unique_id()
	.set_name(str(ID))
	.set_network_master(ID) 	
	print("network init")
	#var info = {}
	#var ID = get_tree().get_network_unique_id()
	#info[ID] = {"name": GLOBAL.playerName, "pieceType": "DINOSAUR", "ID": ID}
	#print("connect to server info ", info)

			
remote func clientSendPlayerInfo():
	print("client reach")
	var info = {}
	var ID = get_tree().get_network_unique_id()
	info[ID] = {"name": GLOBAL.playerName, "pieceType": "DINOSAUR", "ID": ID}
	print("client send info ", info)
	rpc_id(1, "receiveClientPlayerInfo", info)
	

# click on occupied slot
# icon change
# select any location grid to move piece
# return true if moved, false is selected or unselected
func onTextureButtonPressed(button):
	print("occupied X: ", occupiedX)
	print("occupied Y: ", occupiedY)
	print("button X: ", button.getX())
	print("button Y: ", button.getY())

	var dx = abs(occupiedX - button.getX())
	var dy = abs(occupiedY - button.getY())
	
	if(dx > 1 || dy > 1):
		statusBarControl.error("Move is not authorized.")
		return false
		
	if(!isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.highlight()
		isSelected = true
		statusBarControl.normal("")
		return false
		
	if(isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.setPiece()
		isSelected = false
		# statusBarControl.normal("")
		return false
	
	if(isSelected && (button.getX() != occupiedX || button.getY() != occupiedY)):
		button.setPiece()
		board[occupiedX][occupiedY].reset()
		occupiedX = button.getX()
		occupiedY = button.getY()
		isSelected = false
		statusBarControl.normal("Status X: " + str(occupiedX) + " Y: " + str(occupiedY))
		
		return true
		


