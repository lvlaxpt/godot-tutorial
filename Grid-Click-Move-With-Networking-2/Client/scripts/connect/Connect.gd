extends Panel

onready var nameTextEdit = $NameTextEdit
onready var ipAddressTextEdit = $IPAddressTextEdit
onready var button = $Button


# Our PackedScene Instance Reference
var sceneGameInstance: PackedScene

const SERVER_PORT = 1555

# Called when the node enters the scene tree for the first time.
func _ready():
	sceneGameInstance = load("res://Game.tscn")
	button.connect("pressed", self, "connectToServer")
	

func connectToServer():
	GLOBAL.playerName = nameTextEdit.text 
	GLOBAL.IPAddressServer = ipAddressTextEdit.text
	
	var serverIP = GLOBAL.IPAddressServer
	
	"""
	var peer = NetworkedMultiplayerENet.new()
	peer.create_client(serverIP, SERVER_PORT)
	get_tree().network_peer = peer
	"""
	
	#if doesnt change scene works
	#if not doesnt display who is the connect player
	var message = get_tree().change_scene_to(sceneGameInstance)


# chage scene doesnt work
"""
remote func clientSendPlayerInfo():
	var info = {}
	var ID = get_tree().get_network_unique_id()
	info[ID] = {"name": GLOBAL.playerName, "pieceType": "DINOSAUR", "ID": ID}
	print("client send info ", info)
	rpc_id(1, "receiveClientPlayerInfo", info)
	
"""	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
