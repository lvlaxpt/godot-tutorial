extends Panel

onready var ipAddressLabel = $IPAddressLabel
onready var connectedPlayersLabel = $ConnectedPlayersLabel
onready var statusLabel = $StatusLabel
# linux port must be above 1024 to avoid any permission issue
const SERVER_PORT = 1555
const MAX_PLAYERS = 2

var playersInfo = {}
var connectedPlayers: int = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	#pass # Replace with function body.
	
	ipAddressLabel.text = ipAddressLabel.text + " " +getIPAddress()
	connectedPlayersLabel.text = connectedPlayersLabel.text + " " + str(connectedPlayers)
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(SERVER_PORT, 2)

	get_tree().network_peer = peer
	
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")

func _player_disconnected(id):
	pass

func _connected_ok():
	pass
	
func _connected_fail():
	pass

func _server_disconnected():
	pass
	
	
func getIPAddress() -> String:
	for ip in IP.get_local_addresses():
		if "." in ip:
			if !"127" in ip:			
				return ip
				
	return "127.0.0.1"

func _player_connected(ID: int):
	print("ID connected ", ID)
	if(connectedPlayers >= MAX_PLAYERS):
		print("Max players reached. ", MAX_PLAYERS)
		return false
	connectedPlayers = connectedPlayers + 1
	connectedPlayersLabel.text = "Connected Players: " + str(connectedPlayers)
	playersInfo[ID] = {"name": "not set", "pieceType": "not set", "ID": "not set"}
	#rpc_id(ID, "register_player")
	print("reach here")
	rpc_id(ID, "clientSendPlayerInfo")
	print("end reach here")

remote func receiveClientPlayerInfo(info: Dictionary):
	print("client player info", info)
	var keys = info.keys()
	statusLabel.text = statusLabel.text + "Connected Player: " + info[keys[0]].name + "\n"

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
