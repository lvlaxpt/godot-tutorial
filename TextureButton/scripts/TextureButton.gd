
extends TextureButton

const hurt = preload("res://assets/Hurt (6).png")
const walk = preload("res://assets/Walk (1).png")
const slide = preload("res://assets/Slide (10).png")

# custom signal 
# this custom signal will appear automatically at the Node tab
# of texture button
signal custom_pressed(button)

func _ready():
	reset()

func setHurt():
	texture_normal = hurt
	texture_hover = hurt


func setWalk():
	texture_normal = walk
	texture_hover = walk

func reset():
	texture_normal = null
	texture_hover = slide

# signal press is connected to this function 
# the function itself emit custom signal
# in order to emit button as parameter 
# as default press signal doesnt transmit param button
# when button is press, transmit custom_pressed signal 
# together with button class @ self
func _on_TextureButton_custom_pressed():
	emit_signal( "custom_pressed", self )
