extends Control

# $ is shorthand for get_node(path_to_node)
# @see https://godotengine.org/qa/67936/what-does-%24-sign-mean
onready var textureButton = $TextureButton
var isClicked = false

# Called when the node enters the scene tree for the first time.

# <source>.connect(signal, target, target_function)
# if custom_pressed signal is emitted (by clicking a button)
# Control @self onTextureButtonPressed is called and button 
# is passed as parameter
func _ready():
	textureButton.connect( "custom_pressed", self, "onTextureButtonPressed" )



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# change icon based on click
# at first walk icon is displayed when click 
# if click agan hurt icon is displayed
func onTextureButtonPressed(button):
	if(isClicked):
		button.setHurt()
		isClicked = false
	else:
		button.setWalk()
		isClicked = true

