extends Panel

onready var serverCheckButton = $ServerCheckButton
onready var ipAddressLabel = $IPAddressLabel
onready var ipAddressTextEdit = $IPAddressTextEdit
onready var connectButton = $ConnectButton
onready var pieceType = $CowboyDinosaurCheckButton
onready var nameTextEdit = $NameTextEdit
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Our PackedScene Instance Reference
var sceneGameInstance: PackedScene



# Called when the node enters the scene tree for the first time.
func _ready():
	sceneGameInstance = load("res://Main.tscn")
	
	serverCheckButton.connect("pressed", self, "showHideIPAddress")
	connectButton.connect("pressed", self, "gotoGameScene")

func gotoGameScene():
	if(GLOBAL.isServer):
		GLOBAL.playersInfo["server"]["name"] = nameTextEdit.get_text()
	else:
		GLOBAL.playersInfo["client"]["name"] = nameTextEdit.get_text()
		
	if(pieceType.is_pressed()):
		GLOBAL.pieceType = "COWBOY"
		if(GLOBAL.isServer):
			GLOBAL.playersInfo["server"]["pieceType"] = "COWBOY"
		else:
			GLOBAL.playersInfo["client"]["pieceType"] = "COWBOY"
	
	else:
		GLOBAL.pieceType = "DINOSAUR"
		if(GLOBAL.isServer):
			GLOBAL.playersInfo["server"]["pieceType"] = "DINOSAUR"
		else:
			GLOBAL.playersInfo["client"]["pieceType"] = "DINOSAUR"
	
	print("players info ", GLOBAL.playersInfo)		
	var message = get_tree().change_scene_to(sceneGameInstance)


func getIPAddress() -> String:
	for ip in IP.get_local_addresses():
		if "." in ip:
			if !"127" in ip:			
				return ip
				
	return "127.0.0.1"

	
func showHideIPAddress():
	if(serverCheckButton.is_pressed()):
		ipAddressLabel.hide()
		ipAddressTextEdit.hide()
		GLOBAL.isServer = true
		GLOBAL.serverIPAddress = getIPAddress()
		GLOBAL.playersInfo["server"] = {"ID": 1, "name": "wan"}
	else:
		ipAddressLabel.show()
		ipAddressTextEdit.show()
		GLOBAL.isServer = false
		
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
