extends Control

# $ is shorthand for get_node(path_to_node)
# @see https://godotengine.org/qa/67936/what-does-%24-sign-mean
onready var statusBarControl = $StatusBarControl
onready var boardControl = $BoardControl
onready var yourLabel = $YourLabel
onready var otherPlayerLabel = $OtherPlayerLabel

# linux port must be above 1024 to avoid any permission issue
const SERVER_PORT = 1555

var isSelected = false
var occupiedX = 0
var occupiedY = 0
var board = null
var connectedPlayers: int = 0


# Called when the node enters the scene tree for the first time.

# <source>.connect(signal, target, target_function)
# if custom_pressed signal is emitted (by clicking a button)
# Control @self onTextureButtonPressed is called and button 
# is passed as parameter
func _ready():	
			
	var peer = NetworkedMultiplayerENet.new()

	if(GLOBAL.isServer):
		peer.create_server(SERVER_PORT, 1)
		GLOBAL.playersInfo["server"]["ID"] = 1
		statusBarControl.normal("IP Address: " + GLOBAL.serverIPAddress)
		yourLabel.text = yourLabel.text + " (SERVER)"
		yourLabel.text = "Your Name: " + GLOBAL.playersInfo["server"]["name"] + ". Piece Type: " + GLOBAL.pieceType
		otherPlayerLabel.text = "Other Player Name: " + GLOBAL.playersInfo["client"]["name"] + ". Piece Type: " + GLOBAL.playersInfo["server"]["pieceType"]
	
		
	else:
		peer.create_client(GLOBAL.serverIPAddress, SERVER_PORT)
		yourLabel.text = "Your Name: " + GLOBAL.playersInfo["client"]["name"] + ". Piece Type: " + GLOBAL.pieceType
		otherPlayerLabel.text = "Other Player Name: " + GLOBAL.playersInfo["server"]["name"] + ". Piece Type: " + GLOBAL.playersInfo["client"]["pieceType"]
	#init the network
	get_tree().network_peer = peer
	

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")


	board = boardControl.getBoard()
	#spotX0Y0.setPiece()
	print("type ", board[0][0].getType())
	var rowCount = 0
	var colCount = 0
	
	for row in board:
		colCount = 0
		for btn in row:		
			btn.setXY(rowCount, colCount)
			colCount = colCount + 1
			btn.connect( "custom_pressed", self, "onTextureButtonPressed" )
		
		rowCount = rowCount + 1
	
	
func _player_connected(ID: int):
	print("ID connected ", ID)
	rpc_id(ID, "register_player")


remote func register_player():
	# Get the id of the RPC sender.
	#if(connectedPlayers != 0):
	if(connectedPlayers == 1):
		print("Player already connected.")
		return false
	
	connectedPlayers = connectedPlayers + 1
	var ID = get_tree().get_rpc_sender_id()
	# Store the info

	GLOBAL.playersInfo["client"]["ID"] = ID
	print("ID newly connected ", ID)

	var info = "Other Player Name: " + GLOBAL.playersInfo["client"]["name"] + ". Piece Type: " + GLOBAL.playersInfo["client"]["pieceType"]
	print("info b4 rpc ", info)
	rpc_id(1, "updateClientPlayerInfoToServer", 1, info)
	
	info = "Other Player Name: " + GLOBAL.playersInfo["server"]["name"] + ". Piece Type: " + GLOBAL.playersInfo["server"]["pieceType"]
	print("send client ", info)
	rpc_id(ID, "updateClientPlayerInfoToServer", ID, info)
	return true
	
remote func updateClientPlayerInfoToServer(ID: int, info):
	print("info ", info)
	print("ID ", ID)
	otherPlayerLabel.text = info
	#pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# click on occupied slot
# icon change
# select any location grid to move piece
# return true if moved, false is selected or unselected
func onTextureButtonPressed(button):
	print("occupied X: ", occupiedX)
	print("occupied Y: ", occupiedY)
	print("button X: ", button.getX())
	print("button Y: ", button.getY())

	var dx = abs(occupiedX - button.getX())
	var dy = abs(occupiedY - button.getY())
	
	if(dx > 1 || dy > 1):
		statusBarControl.error("Move is not authorized.")
		return false
		
	if(!isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.highlight()
		isSelected = true
		statusBarControl.normal("")
		return false
		
	if(isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.setPiece()
		isSelected = false
		# statusBarControl.normal("")
		return false
	
	if(isSelected && (button.getX() != occupiedX || button.getY() != occupiedY)):
		button.setPiece()
		board[occupiedX][occupiedY].reset()
		occupiedX = button.getX()
		occupiedY = button.getY()
		isSelected = false
		statusBarControl.normal("Status X: " + str(occupiedX) + " Y: " + str(occupiedY))
		
		return true
		


