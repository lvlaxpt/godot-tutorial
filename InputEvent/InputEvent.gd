extends Panel

# docs reference 
# https://docs.godotengine.org/en/stable/tutorials/inputs/inputevent.html
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_pressed("ui_right"):
		# Right arrow is pressed.
		print("Move right")
	if Input.is_action_pressed("ui_left"):
		# Left key arrow is pressed.
		print("Move left")
	
#	pass


func _input(event):
	# print if mouse button is click either left, right button or double click
	if event is InputEventMouseButton:
		print("mouse position " , event.position)
		print("mouse double click " , event.doubleclick)
		
	if Input.is_action_pressed("ui_mouse_left_button_click"):
		print("Mouse left button click")
		
# press escape and the game will quit
func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ESCAPE:
			get_tree().quit()
