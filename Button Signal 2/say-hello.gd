extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#pass # Replace with function body
	get_node("Button").connect("pressed", self, "_on_Button_pressed")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_pressed():
	# pass # Replace with function body.
	var text = get_node("Label").text
	
	if(text == "This is label" || text == "SELAMAT!"):
			get_node("Label").text = "HELLO!"
	else:
		get_node("Label").text = "SELAMAT!"

