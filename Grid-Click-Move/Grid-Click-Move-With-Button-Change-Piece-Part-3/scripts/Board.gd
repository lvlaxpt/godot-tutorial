extends Control

# $ is shorthand for get_node(path_to_node)
# @see https://godotengine.org/qa/67936/what-does-%24-sign-mean
onready var statusBarControl = $StatusBarControl
onready var boardControl = $BoardControl
onready var cowboyBtn = $CowboyButton
onready var dinosaurBtn = $DinosaurButton


var isSelected = {}
var isToggledCowboy = false
var isToggledDinosaur = false
var occupiedSpot = {}
var board = null

# Called when the node enters the scene tree for the first time.

# <source>.connect(signal, target, target_function)
# if custom_pressed signal is emitted (by clicking a button)
# Control @self onTextureButtonPressed is called and button 
# is passed as parameter
func _ready():	
			
	cowboyBtn.connect( "toggleCowboy", self, "toggleCowboy" )
	dinosaurBtn.connect( "toggleDinosaur", self, "toggleDinosaur" )
	board = boardControl.getBoard()

	var rowCount = 0
	var colCount = 0
	isSelected["COWBOY"] = false
	isSelected["DINOSAUR"] = false
	
	occupiedSpot["COWBOY_X"] = 0
	occupiedSpot["COWBOY_Y"] = 0
	occupiedSpot["DINOSAUR_X"] = 2
	occupiedSpot["DINOSAUR_Y"] = 2
	
	for row in board:
		colCount = 0
		for btn in row:		
			btn.setXY(rowCount, colCount)
			colCount = colCount + 1
			btn.connect( "custom_pressed", self, "onTextureButtonPressed" )
		
		rowCount = rowCount + 1
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func toggleCowboy():
	for row in board:
		for btn in row:		
			if(btn.getType() == "COWBOY"):
				if(isToggledCowboy):
					btn.highlight()
				else: 
					btn.setPiece()
				isToggledCowboy = !isToggledCowboy
		
func toggleDinosaur():
	for row in board:
		for btn in row:		
			if(btn.getType() == "DINOSAUR"):
				if(isToggledDinosaur):
					btn.highlight()
				else: 
					btn.setPiece()
				isToggledDinosaur = !isToggledDinosaur

# click on occupied slot
# icon change
# select any location grid to move piece
# return true if moved, false is selected or unselected

func onTextureButtonPressed(button):
	print("button X: ", button.getX())
	print("button Y: ", button.getY())
	print("button type: ", button.getType())
	
	statusBarControl.normal("")
	
	if(isSelected["COWBOY"] == false && button.getType() == "COWBOY" && isSelected["DINOSAUR"] == false):
		isSelected["COWBOY"] = true
		button.highlight()
		return false
		
	if(isSelected["COWBOY"] == true && button.getType() == "COWBOY"):
		isSelected["COWBOY"] = false
		button.setPiece()
		return false
			
	if(isSelected["DINOSAUR"] == false && button.getType() == "DINOSAUR" && isSelected["COWBOY"] == false):
		isSelected["DINOSAUR"] = true
		button.highlight()
		return false
	
	if(isSelected["DINOSAUR"] == true && button.getType() == "DINOSAUR"):
		isSelected["DINOSAUR"] = false
		button.setPiece()
		return false
	
	# move to empty spot
	#var pieceType = button.getType()
	var dx = 0
	var dy = 0
	
	if(isSelected["COWBOY"] == true):
		dx = abs(occupiedSpot["COWBOY_X"] - button.getX())
		dy = abs(occupiedSpot["COWBOY_Y"] - button.getY())

	if(isSelected["DINOSAUR"] == true):
		dx = abs(occupiedSpot["DINOSAUR_X"] - button.getX())
		dy = abs(occupiedSpot["DINOSAUR_Y"] - button.getY())
			
	if(dx > 1 || dy > 1):
		statusBarControl.error("Move is not authorized.")
		return false
		
	if(!button.hasPiece() && isSelected["COWBOY"] == true && (button.getX() != occupiedSpot["COWBOY_X"] || button.getY() != occupiedSpot["COWBOY_Y"])):
		button.setPiece2("COWBOY")
		board[occupiedSpot["COWBOY_X"]][occupiedSpot["COWBOY_Y"]].reset()
		occupiedSpot["COWBOY_X"] = button.getX()
		occupiedSpot["COWBOY_Y"] = button.getY()
		isSelected["COWBOY"] = false
		statusBarControl.normal("Status X: " + str(occupiedSpot["COWBOY_X"]) + " Y: " + str(occupiedSpot["COWBOY_Y"]))
		
		return true
		
	if(!button.hasPiece() && isSelected["DINOSAUR"] == true && (button.getX() != occupiedSpot["DINOSAUR_X"] || button.getY() != occupiedSpot["DINOSAUR_Y"])):
		button.setPiece2("DINOSAUR")
		board[occupiedSpot["DINOSAUR_X"]][occupiedSpot["DINOSAUR_Y"]].reset()
		occupiedSpot["DINOSAUR_X"] = button.getX()
		occupiedSpot["DINOSAUR_Y"] = button.getY()
		isSelected["DINOSAUR"] = false
		statusBarControl.normal("Status X: " + str(occupiedSpot["DINOSAUR_X"]) + " Y: " + str(occupiedSpot["DINOSAUR_Y"]))
		
		return true
