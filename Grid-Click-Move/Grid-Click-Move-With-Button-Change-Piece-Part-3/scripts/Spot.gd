extends TextureButton

const pieceCowboy = preload("res://assets/idle-small.png")
const highlight = preload("res://assets/target.png")
const moveCowboy = preload("res://assets/run-small.png")

const pieceDinosaur = preload("res://assets/dragon-stand.png")
const moveDinosaur = preload("res://assets/dragon-run.png")
# custom signal 
# this custom signal will appear automatically at the Node tab
# of texture button
signal custom_pressed(button)

var x = 0
var y = 0
var type: String = "EMPTY"

func setType(type: String):
	self.type = type
	
func getType() -> String:
	return self.type
	
func _ready():
	reset()
	
func setXY(x: int, y: int):
	self.x = x
	self.y = y

func getX() -> int:
	return self.x

func getY() -> int:
	return self.y
	
func highlight():
	if(self.type == "COWBOY"):
		texture_normal = moveCowboy 
		texture_hover = moveCowboy
		
	if(self.type == "DINOSAUR"):
		texture_normal = moveDinosaur 
		texture_hover = moveDinosaur
	
func run():
	if(self.type == "COWBOY"):
		texture_normal = moveCowboy
		texture_hover = moveCowboy
	
	
func setPiece():
	if(self.type == "COWBOY"):
		texture_normal = pieceCowboy
		texture_hover = pieceCowboy
	else:
		texture_normal = pieceDinosaur
		texture_hover = pieceDinosaur


	
func setPiece2(type: String):
	self.type = type
	if(type == "COWBOY"):
		texture_normal = pieceCowboy
		texture_hover = pieceCowboy
		
	if (type == "DINOSAUR"):
		texture_normal = pieceDinosaur
		texture_hover = pieceDinosaur

func reset():
	self.type = "EMPTY"
	texture_normal = null
	texture_hover = highlight

func hasPiece() -> bool:
	return self.type != "EMPTY"
	
# signal press is connected to this function 
# the function itself emit custom signal
# in order to emit button as parameter 
# as default press signal doesnt transmit param button
# when button is press, transmit custom_pressed signal 
# together with button class @ self
func _on_TextureButton_custom_pressed():
	emit_signal( "custom_pressed", self )
