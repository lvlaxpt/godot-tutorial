extends Button

signal toggleDinosaur()
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	# var new_style = StyleBoxFlat.new()
	#new_style.set_bg_color(Color("#FFFFFF"))
	# new_style.set_bg_color(Color(1, 1, 1, 1))
	#set("custom_styles/normal.bg_color", new_style)
	#.["custom_styles/normal"].bg_color = Color("#FFFFFF")
	#texture_normal
	# theme.set_color("bg_color", "Button", Color(1, 1, 1, 1))
	# pass # Replace with function body.
	var customTheme = Theme.new()
	customTheme.set_color("normal", "Button", Color("#00FF00"))
	theme = customTheme


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_DinosaurButton_pressed():
	emit_signal("toggleDinosaur")
	#pass # Replace with function body.
