extends Control

# $ is shorthand for get_node(path_to_node)
# @see https://godotengine.org/qa/67936/what-does-%24-sign-mean
onready var statusBarControl = $StatusBarControl
onready var boardControl = $BoardControl

var isSelected = false
var occupiedX = 0
var occupiedY = 0
var board = null

# Called when the node enters the scene tree for the first time.

# <source>.connect(signal, target, target_function)
# if custom_pressed signal is emitted (by clicking a button)
# Control @self onTextureButtonPressed is called and button 
# is passed as parameter
func _ready():	
			
	board = boardControl.getBoard()
	#spotX0Y0.setPiece()
	board[0][0].setPiece()
	var rowCount = 0
	var colCount = 0
	
	for row in board:
		colCount = 0
		for btn in row:		
			btn.setXY(rowCount, colCount)
			colCount = colCount + 1
			btn.connect( "custom_pressed", self, "onTextureButtonPressed" )
		
		rowCount = rowCount + 1
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# click on occupied slot
# icon change
# select any location grid to move piece
# return true if moved, false is selected or unselected
func onTextureButtonPressed(button):
	print("occupied X: ", occupiedX)
	print("occupied Y: ", occupiedY)
	print("button X: ", button.getX())
	print("button Y: ", button.getY())

	var dx = abs(occupiedX - button.getX())
	var dy = abs(occupiedY - button.getY())
	
	if(dx > 1 || dy > 1):
		statusBarControl.error("Move is not authorized.")
		return false
		
	if(!isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.highlight()
		isSelected = true
		statusBarControl.normal("")
		return false
		
	if(isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.setPiece()
		isSelected = false
		# statusBarControl.normal("")
		return false
	
	if(isSelected && (button.getX() != occupiedX || button.getY() != occupiedY)):
		button.setPiece()
		board[occupiedX][occupiedY].reset()
		occupiedX = button.getX()
		occupiedY = button.getY()
		isSelected = false
		statusBarControl.normal("Status X: " + str(occupiedX) + " Y: " + str(occupiedY))
		
		return true
		


