extends Control

# $ is shorthand for get_node(path_to_node)
# @see https://godotengine.org/qa/67936/what-does-%24-sign-mean
onready var spotX0Y0 = $SpotX0Y0
onready var spotX1Y0 = $SpotX1Y0
onready var spotX2Y0 = $SpotX2Y0

onready var spotX0Y1 = $SpotX0Y1
onready var spotX1Y1 = $SpotX1Y1
onready var spotX2Y1 = $SpotX2Y1

onready var spotX0Y2 = $SpotX0Y2
onready var spotX1Y2 = $SpotX1Y2
onready var spotX2Y2 = $SpotX2Y2

onready var board = [ [spotX0Y0, spotX1Y0, spotX2Y0], [spotX0Y1, spotX1Y1, spotX2Y1], [spotX0Y2, spotX1Y2, spotX2Y2]]
var isSelected = false

var occupiedX = 0
var occupiedY = 0

# Called when the node enters the scene tree for the first time.

# <source>.connect(signal, target, target_function)
# if custom_pressed signal is emitted (by clicking a button)
# Control @self onTextureButtonPressed is called and button 
# is passed as parameter
func _ready():	
			
	spotX0Y0.setPiece()
	var rowCount = 0
	var colCount = 0
	
	for row in board:

		colCount = 0
		for btn in row:		
			btn.setXY(rowCount, colCount)
			colCount = colCount + 1
			btn.connect( "custom_pressed", self, "onTextureButtonPressed" )
		
		rowCount = rowCount + 1
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

# click on occupied slot
# icon change
# select any location grid to move piece
# return true if moved, false is selected or unselected
func onTextureButtonPressed(button):
	print("occupied X: ", occupiedX)
	print("occupied Y: ", occupiedY)
	print("button X: ", button.getX())
	print("button Y: ", button.getY())
		
	if(!isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.highlight()				
		isSelected = true
		return false
		
	if(isSelected && button.getX() == occupiedX && button.getY() == occupiedY):
		button.setPiece()
		isSelected = false
		return false
	
	if(isSelected && (button.getX() != occupiedX || button.getY() != occupiedY)):
		button.setPiece()
		board[occupiedX][occupiedY].reset()
		occupiedX = button.getX()
		occupiedY = button.getY()
		isSelected = false
		return true
		


