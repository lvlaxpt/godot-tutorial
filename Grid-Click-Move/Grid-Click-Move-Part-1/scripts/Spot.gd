extends TextureButton

const piece = preload("res://assets/idle-small.png")
const highlight = preload("res://assets/target.png")
const move = preload("res://assets/run-small.png")

# custom signal 
# this custom signal will appear automatically at the Node tab
# of texture button
signal custom_pressed(button)

var x = 0
var y = 0
var text: String = ""

func _ready():
	reset()

func getName() -> String:
	return self.text 
	
func setName(name):
	self.text = name
	
func setXY(x: int, y: int):
	self.x = x
	self.y = y

func getX() -> int:
	return self.x

func getY() -> int:
	return self.y
	
func highlight():
	texture_normal = move 
	texture_hover = move
	
func run():
	texture_normal = move
	texture_hover = move
	
	
func setPiece():
	texture_normal = piece
	texture_hover = piece

func reset():
	texture_normal = null
	texture_hover = highlight


# signal press is connected to this function 
# the function itself emit custom signal
# in order to emit button as parameter 
# as default press signal doesnt transmit param button
# when button is press, transmit custom_pressed signal 
# together with button class @ self
func _on_TextureButton_custom_pressed():
	print("press texture")
	emit_signal( "custom_pressed", self )
