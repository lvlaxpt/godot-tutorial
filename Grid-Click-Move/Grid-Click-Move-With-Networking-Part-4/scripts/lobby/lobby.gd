extends Control


# Called when the node enters the scene tree for the first time.
func _ready():

	#connection_succeeded emitted if a client connect to server only
	#change Connect panel to Players panel
	GameState.connect("connection_succeeded", self, "_on_connection_success")
	
	GameState.connect("player_list_changed", self, "refresh_lobby")
	
	$Connect/NameLineEdit.text = _get_OS_username()


func _get_OS_username() -> String:
	if OS.has_environment("USERNAME"):
		return OS.get_environment("USERNAME")
	else:
		var desktop_path = OS.get_system_dir(0).replace("\\", "/").split("/")
		return desktop_path[desktop_path.size() - 2]
	
func _on_start_pressed():
	GameState.begin_game()
			
func _on_host_pressed():
	if $Connect/NameLineEdit.text == "":
		$Connect/ErrorLabel.text = "Invalid name!"
		return

	$Connect.hide()
	$Players.show()
	$Connect/ErrorLabel.text = ""

	var player_name = $Connect/NameLineEdit.text
	GameState.host_game(player_name)
	refresh_lobby()


func _on_join_pressed():
	if $Connect/NameLineEdit.text == "":
		$Connect/ErrorLabel.text = "Invalid name!"
		return

	var ip = $Connect/IPAddressLineEdit.text
	if not ip.is_valid_ip_address():
		$Connect/ErrorLabel.text = "Invalid IP address!"
		return

	$Connect/ErrorLabel.text = ""
	$Connect/HostButton.disabled = true
	$Connect/JoinButton.disabled = true

	var player_name = $Connect/NameLineEdit.text
	GameState.join_game(ip, player_name)
	print("lobby on join press end. ", " time stamp: ", OS.get_system_time_msecs())
	

func refresh_lobby():
	var players = GameState.get_player_list()
	print("lobby -  refresh_lobby() Players: ", players, " time stamp: ", OS.get_system_time_msecs())
	players.sort()
	$Players/List.clear()
	$Players/List.add_item(GameState.get_player_name() + " (You)")
	for p in players:
		$Players/List.add_item(p)

	$Players/Start.disabled = not get_tree().is_network_server()

func _on_connection_success():
	print("lobby - _on_connect_success()", " time stamp: ", OS.get_system_time_msecs())
	$Connect.hide()
	$Players.show()
