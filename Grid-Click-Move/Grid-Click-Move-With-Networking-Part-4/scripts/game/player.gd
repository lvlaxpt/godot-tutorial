extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var position2D: Vector2 = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func idle():
	$idle.show()
	$run.hide()

func run():
	$idle.hide()
	$run.show()
	
func getIdleTexture() -> Texture:
	return $idle.texture

func getRunTexture() -> Texture:
	return $run.texture
	

func getType()-> String:
	return $type.text

func setPosition(position: Vector2):
	self.position2D = position
	
func getX():
	return position2D.x

func getY():
	return position2D.y
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
