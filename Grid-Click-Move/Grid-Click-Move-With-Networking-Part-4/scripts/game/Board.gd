extends Control

# $ is shorthand for get_node(path_to_node)
# @see https://godotengine.org/qa/67936/what-does-%24-sign-mean
onready var statusBarControl = $StatusBarControl
onready var boardControl = $BoardControl
onready var cowboyBtn = $CowboyButton
onready var dinosaurBtn = $DinosaurButton

# todo article
# rset arr
# rset obj
# rset node

var isToggledCowboy = false
var isToggledDinosaur = false

var board = null
var isSelected: bool = false

puppet var next = {}
puppet var nextMoves = []
# Called when the node enters the scene tree for the first time.

# <source>.connect(signal, target, target_function)
# if custom_pressed signal is emitted (by clicking a button)
# Control @self onTextureButtonPressed is called and button 
# is passed as parameter
func _ready():	
			
	cowboyBtn.connect( "toggleCowboy", self, "toggleCowboy" )
	dinosaurBtn.connect( "toggleDinosaur", self, "toggleDinosaur" )
	board = boardControl.getBoard()
	
	# @see https://godotengine.org/qa/61680/error-message-on-rset-mode-is-0-master-is-1
	rset_config("next", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	rset_config("nextMoves", MultiplayerAPI.RPC_MODE_REMOTESYNC)
	
	var rowCount = 0
	var colCount = 0

	#nextMoves.insert(0,{"position": Vector2(-1, -1)})
	
	for row in board:
		colCount = 0
		for btn in row:		
			btn.setXY(rowCount, colCount)
			colCount = colCount + 1
			btn.connect( "custom_pressed", self, "onTextureButtonPressed" )
		
		rowCount = rowCount + 1


func addPlayer(playerType: String, position: Vector2):
	board[position.x][position.y].setPlayerType(playerType)
	board[position.x][position.y].normal()


func toggleCowboy():
	for row in board:
		for btn in row:		
			if(btn.getType() == "COWBOY"):
				if(isToggledCowboy):
					btn.highlight()
				else: 
					btn.setPiece()
				isToggledCowboy = !isToggledCowboy
		
func toggleDinosaur():
	for row in board:
		for btn in row:		
			if(btn.getType() == "DINOSAUR"):
				if(isToggledDinosaur):
					btn.highlight()
				else: 
					btn.setPiece()
				isToggledDinosaur = !isToggledDinosaur

func _process(delta):
	#pass
	#if(nextMove != null):
	#	print("move ", nextMove.getType())
		#print("move ", nextMove)
	#print("next ", next)
	#if(board[next["position"].x][next["position"].y].hasPiece()):
	#board[next["position"].x][next["position"].y].runAction(next["action"])
	#if(!nextMoves.empty()):
	#	print("next arr", nextMoves)
	#	nextMoves.clear()
	
	for item in nextMoves:
		var playerType = "EMPTY"
		
		if(item.has("playerType")):
			playerType = item["playerType"]
			
		board[item["position"].x][item["position"].y].runAction(item["action"], playerType)
	


# click on occupied slot
# icon change
# select any location grid to move piece
# return true if moved, false is selected or unselected
func onTextureButtonPressed(button):
	print("button X: ", button.getX())
	print("button Y: ", button.getY())
	
	var network_id = get_tree().get_network_unique_id()
	var nextMv = {}
	var nextArr = []
	
	if(network_id == GameState.player_network_id && GameState.player_type[network_id] == button.getType()):

		statusBarControl.normal("")

		if(!isSelected && button.hasPiece()): 
			isSelected = true
			nextMv["position"] = Vector2(button.getX(), button.getY())
			nextMv["action"] = "HIGHLIGHT"

			nextArr.insert(0, nextMv)
			rset("next", nextMv)
			rset("nextMoves", nextArr)
			
			return false
			
		if(isSelected && next["position"].x == button.getX() &&  next["position"].y == button.getY()):
			isSelected = false
			nextMv["position"] = Vector2(button.getX(), button.getY())
			nextMv["action"] = "NORMAL"
			
			nextArr.insert(0, nextMv)
			rset("next", nextMv)
			rset("nextMoves", nextArr)
			

			return false
	
	if(network_id == GameState.player_network_id && isSelected && button.hasPiece()):
			statusBarControl.error("Destination spot has already a piece.")
			return false	
	
	if(network_id == GameState.player_network_id && isSelected && !button.hasPiece()):
		var dx = abs(next["position"].x - button.getX())
		var dy = abs(next["position"].y - button.getY())


		if(dx > 1 || dy > 1):
			statusBarControl.error("Move is not authorized.")
			return false
			
		if(button.getX() != next["position"].x || button.getY() != next["position"].y):
			var playerType = board[nextMoves[0]["position"].x][nextMoves[0]["position"].y].getType()
			
			nextMoves[0]["action"] = "REMOVE"
			nextMoves.append({"position": Vector2(button.getX(), button.getY()), "action": "NORMAL", "playerType": playerType})
			isSelected = false
			
			rset("next", nextMv)
			rset("nextMoves", nextMoves)
			
			statusBarControl.normal("Status X: " + str(button.getX()) + " Y: " + str(button.getY()))
			return true

