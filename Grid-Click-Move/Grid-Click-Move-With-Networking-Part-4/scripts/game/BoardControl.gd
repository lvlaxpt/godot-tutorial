extends Control

onready var spotX0Y0 = $SpotX0Y0
onready var spotX1Y0 = $SpotX1Y0
onready var spotX2Y0 = $SpotX2Y0

onready var spotX0Y1 = $SpotX0Y1
onready var spotX1Y1 = $SpotX1Y1
onready var spotX2Y1 = $SpotX2Y1

onready var spotX0Y2 = $SpotX0Y2
onready var spotX1Y2 = $SpotX1Y2
onready var spotX2Y2 = $SpotX2Y2


onready var board = [ [spotX0Y0, spotX1Y0, spotX2Y0], [spotX0Y1, spotX1Y1, spotX2Y1], [spotX0Y2, spotX1Y2, spotX2Y2]]



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func getBoard():
	return board

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
