extends TextureButton

const target = preload("res://assets/target.png")
# custom signal 
# this custom signal will appear automatically at the Node tab
# of texture button
signal custom_pressed(button)


const COWBOY: String = "COWBOY"
const DINOSAUR: String = "DINOSAUR"
const EMPTY: String = "EMPTY"

var x = 0
var y = 0
var type: String = EMPTY
var action: String = "NONE"

var textureNormal: Texture = null
var textureHover: Texture = null
var player_scenes = {}



func getType() -> String:
	return self.type

	
func _ready():
	reset()
	player_scenes[COWBOY] = load("res://screens/cowboy.tscn").instance()
	player_scenes[DINOSAUR] = load("res://screens/dinosaur.tscn").instance()
	
func setXY(x: int, y: int):
	self.x = x
	self.y = y

func getX() -> int:
	return self.x

func getY() -> int:
	return self.y
	
func run():
	pass

	
func highlight():
	texture_normal = self.textureHover
	texture_hover = self.textureHover
	
func normal():
	texture_normal = self.textureNormal
	texture_hover = self.textureNormal
	
		
func reset():
	self.type = EMPTY
	texture_normal = null
	texture_hover = target

func hasPiece() -> bool:
	return self.type != EMPTY

	
func setPlayerType(type: String):
	self.type = type
	
	if(self.type == COWBOY):
		self.textureNormal = player_scenes[COWBOY].getIdleTexture()
		self.textureHover = player_scenes[COWBOY].getRunTexture()
		
	if(self.type == DINOSAUR):
		self.textureNormal = player_scenes[DINOSAUR].getIdleTexture()
		self.textureHover = player_scenes[DINOSAUR].getRunTexture()
	
	
	
func runAction(action: String, playerType: String = EMPTY):
	print("run action player type: ", playerType)
	
	if(playerType != EMPTY):
		setPlayerType(playerType)
		
	if(action == "NONE"):
		return false
	
	if(action == "HIGHLIGHT"):
		highlight()

	if(action == "NORMAL"):
		normal()
	
	if(action == "REMOVE"):
		reset()

	action = "NONE"
	return true
	
# signal press is connected to this function 
# the function itself emit custom signal
# in order to emit button as parameter 
# as default press signal doesnt transmit param button
# when button is press, transmit custom_pressed signal 
# together with button class @ self
func _on_TextureButton_custom_pressed():
	emit_signal( "custom_pressed", self )
