extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _init():
	normal()
	# highlight()
	
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func normal():
	var new_style = StyleBoxFlat.new()
	new_style.set_bg_color(Color(0, 0, 0, 1))
	set("custom_styles/panel", new_style)
	
func error():
	var new_style = StyleBoxFlat.new()
	new_style.set_bg_color(Color(1, 0, 0, 1))
	set("custom_styles/panel", new_style)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
