extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var statusBarPanel = $StatusBarPanel
onready var statusBar = $StatusBar

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func normal(text):
	statusBar.text = text
	statusBarPanel.normal()
	
func error(text): 
	statusBar.text = text
	statusBarPanel.error()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
