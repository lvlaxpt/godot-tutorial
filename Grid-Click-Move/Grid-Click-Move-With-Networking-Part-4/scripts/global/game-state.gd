extends Node

# Default game server port. Can be any number between 1024 and 49151.
# Not on the list of registered or common ports as of November 2020:
# https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers
const DEFAULT_PORT = 10567

# Max number of players.
const MAX_PEERS = 12

# Name for my player.
var player_name = "The Warrior"
var peer = null

# Names for remote players in id:name format.
var players = {}
var players_ready = []

var player_network_id = -1
var player_type = {}
# Signals to let lobby GUI know what's going on.
signal connection_succeeded()
signal player_list_changed()

# Called when the node enters the scene tree for the first time.
func _ready():
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("connected_to_server", self, "_connected_ok")


# Callback from SceneTree, only for clients (not server).
func _connected_ok():
	# We just connected to a server
	emit_signal("connection_succeeded")

# Callback from SceneTree.
func _player_connected(id):
	# Registration of a client beings here, tell the connected player that we are here.
	rpc_id(id, "register_player", player_name)


# Lobby management functions.
remote func register_player(new_player_name):
	var id = get_tree().get_rpc_sender_id()
	players[id] = new_player_name
	emit_signal("player_list_changed")


func host_game(new_player_name):
	player_name = new_player_name
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(DEFAULT_PORT, MAX_PEERS)
	get_tree().set_network_peer(peer)
	player_network_id = get_tree().get_network_unique_id()
	player_type[player_network_id] = "COWBOY"
	print("host network id ", get_tree().get_network_unique_id())


func join_game(ip, new_player_name):
	player_name = new_player_name
	peer = NetworkedMultiplayerENet.new()
	peer.create_client(ip, DEFAULT_PORT)
	get_tree().set_network_peer(peer)
	print("join network id ", get_tree().get_network_unique_id())
	player_network_id = get_tree().get_network_unique_id()
	player_type[player_network_id] = "DINOSAUR"

func get_player_list():
	return players.values()


func get_player_name():
	return player_name
	
func begin_game():
	print("begin game")
	assert(get_tree().is_network_server())

	# Create a dictionary with peer id and respective spawn points, 
	# could be improved by randomizing.
	var spawn_points = {}
	spawn_points[1] = Vector2(0, 0) #server spawn at point 0, 0
	#var spawn_point_idx = 1
	#assuming 1 client player only
	for p in players:
		spawn_points[p] = Vector2(2, 2)


	# Call to pre-start game with the spawn points.
	for p in players:
		rpc_id(p, "pre_start_game", spawn_points)

	pre_start_game(spawn_points)

remote func pre_start_game(spawn_points):
	# Change scene.
	var world = load("res://screens/world.tscn").instance()
	get_tree().get_root().add_child(world)
	get_tree().get_root().get_node("Lobby").hide()

	for p_id in spawn_points:			
		var playerType = "COWBOY"	
		if(p_id != 1):
			playerType = "DINOSAUR"
			
		world.addPlayer(playerType, spawn_points[p_id])

	if not get_tree().is_network_server():
		# Tell server we are ready to start.
		rpc_id(1, "ready_to_start", get_tree().get_network_unique_id())
	elif players.size() == 0:
		post_start_game()



remote func ready_to_start(id):
	assert(get_tree().is_network_server())

	if not id in players_ready:
		players_ready.append(id)

	if players_ready.size() == players.size():
		for p in players:
			rpc_id(p, "post_start_game")
		post_start_game()


remote func post_start_game():
	get_tree().set_pause(false) # Unpause and unleash the game!
