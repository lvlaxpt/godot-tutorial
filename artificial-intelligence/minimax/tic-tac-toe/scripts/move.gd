extends Node

class_name Move
# store position
# e.g Move["x"] = 1, Move["y"] = 2
var move: Dictionary = {}

func _init():
	move["x"] = -1
	move["y"] = -1

func setPosition(x: int, y: int):
	move["x"] = x
	move["y"] = y

func getPosition() -> Dictionary:
	return move
	
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
