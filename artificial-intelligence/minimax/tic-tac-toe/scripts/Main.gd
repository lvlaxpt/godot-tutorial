extends Panel


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var board = Board.new()
	
	# X O X
	# O O X
	# - - - 
	# result (2, 2)
	"""
	board.setX(0, 0)
	board.setO(1, 0)
	board.setX(2, 0)
	
	board.setO(0, 1)
	board.setO(1, 1)
	board.setX(2, 1)
	"""
	
	# X O X
	# O X O
	# - - - 
	# result (2, 0)
	"""
	board.setX(0, 0)
	board.setO(1, 0)
	board.setX(2, 0)
	
	board.setO(0, 1)
	board.setX(1, 1)
	board.setO(2, 1)
	"""
	
	# O X O
	# X X O
	# - - - 
	#result (2, 1)
	"""
	board.setO(0, 0)
	board.setX(1, 0)
	board.setO(2, 0)
	
	board.setX(0, 1)
	board.setX(1, 1)
	board.setO(2, 1)
	"""
	
	# X X -
	# X O O
	# O - - 
	#result (0, 2)
	"""
	board.setX(0, 0)
	board.setX(1, 0)
	
	board.setX(0, 1)
	board.setO(1, 1)
	board.setO(2, 1)
	
	board.setO(0, 2)
	"""
	
	# X already win
	# X X X
	# X O O
	# O - O 
	# result (-1, -1) means already win or draw
	"""
	board.setX(0, 0)
	board.setX(1, 0)
	board.setX(2, 0)
	
	board.setX(0, 1)
	board.setO(1, 1)
	board.setO(2, 1)
	
	board.setO(0, 2)
	board.setO(2, 2)
	"""
	
	#O already win
	# X - X
	# X X O
	# O O O 
	# result (-1, -1) means already win or draw
	"""
	board.setX(0, 0)
	board.setX(2, 0)
	
	board.setX(0, 1)
	board.setX(1, 1)
	board.setO(2, 1)
	
	board.setO(0, 2)
	board.setO(1, 2)
	board.setO(2, 2)
	"""
	
	
	
	#O already win
	# X O X
	# O X O
	# O X O 
	# result (-1, -1) means already win or draw
	
	board.setX(0, 0)
	board.setO(1, 0)
	board.setX(2, 0)
	
	board.setO(0, 1)
	board.setX(1, 1)
	board.setO(2, 1)
	
	board.setO(0, 2)
	board.setX(1, 2)
	board.setO(2, 2)
	
	board.print()
	
	var ai = AIMinimax.new()
	var bestMove = ai.findBestMove(board)
 
	print("The Optimal Move is :")
	print("ROW:", bestMove["y"], " COL:", bestMove["x"])
	print("get type ", board.getType(0, 0))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
