extends Node

class_name AIMinimax
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func evaluate(board: Board):
	# Checking for Rows for X or O victory.
	for row in range(board.MAX_WIDTH) :    
		if (board.getType(row, 0) == board.getType(row, 1) and board.getType(row, 1) == board.getType(row, 2)) :       
			if (board.getType(row, 0) == board.CROSS) :
				return board.CROSS_WIN
			elif (board.getType(row, 0) == board.CIRCLE) :
				return board.CIRCLE_WIN
 
	# Checking for Columns for X or O victory.
	for col in range(board.MAX_HEIGHT) :
	  
		if (board.getType(0, col) == board.getType(1, col) and board.getType(1, col) == board.getType(2, col)) :
		 
			if (board.getType(0, col) == board.CROSS) :
				return board.CROSS_WIN
			elif (board.getType(0, col) == board.CIRCLE) :
				return board.CIRCLE_WIN
 
	# Checking for Diagonals for X or O victory.
	if (board.getType(0, 0) == board.getType(1, 1) and board.getType(1, 1) == board.getType(2, 2)) :
	 
		if (board.getType(0, 0) == board.CROSS) :
			return board.CROSS_WIN
		elif (board.getType(0, 0) == board.CIRCLE) :
			return board.CIRCLE_WIN
 
	if (board.getType(0, 2) == board.getType(1, 1) and board.getType(1, 1) == board.getType(2, 0)) :
	 
		if (board.getType(0, 2) == board.CROSS) :
			return board.CROSS_WIN
		elif (board.getType(0, 2) == board.CIRCLE) :
			return board.CIRCLE_WIN
 
	# Else if none of them have won then return 0
	return board.DRAW
	
# This is the minimax function. It considers all
# the possible ways the game can go and returns
# the value of the board
func minimax(board: Board, depth: int, isMax: bool) :
	var score = evaluate(board)
	var best = 0
	#print("minimax score ", score)
	#board.print()
	# If Maximizer has won the game return his/her
	# evaluated score
	if (score == 10) :
		return board.CROSS_WIN
 
	# If Minimizer has won the game return his/her
	# evaluated score
	if (score == -10) :
		return board.CIRCLE_WIN
 
	# If there are no more moves and no winner then
	# it is a tie
	if(!board.hasMoreMoves()):
		return board.DRAW
 
	# If this maximizer's move
	if (isMax) :    
		best = -1000
 
		# Traverse all cells
		for i in range(board.MAX_WIDTH) :        
			for j in range(board.MAX_HEIGHT) :
			  
				# Check if cell is empty
				if(board.isEmpty(i, j)):
					# Make the move
					#board[i][j] = player
					board.setX(i, j)
 
					# Call minimax recursively and choose
					# the maximum value
					best = max( best, minimax(board, depth + 1, not isMax) )
 
					# Undo the move
					#board[i][j] = '_'
					board.setEmpty(i, j)
		return best
 
	# If this minimizer's move
	else :
		best = 1000
 
		# Traverse all cells
		for i in range(board.MAX_WIDTH) :        
			for j in range(board.MAX_HEIGHT) :
			  
				# Check if cell is empty
				if(board.isEmpty(i, j)):
					# Make the move
					#board[i][j] = opponent
					board.setO(i, j)
 
					# Call minimax recursively and choose
					# the minimum value
					best = min(best, minimax(board, depth + 1, not isMax))
 
					# Undo the move
					#board[i][j] = '_'
					board.setEmpty(i, j)
		return best
 
func findBestMove(board: Board) :
	var bestVal = -1000
	var bestMove: Move = Move.new()
	#print("status ", evaluate(board))
	var status = evaluate(board)
	
	if(status == board.CIRCLE_WIN || status == board.CROSS_WIN || status == board.DRAW):
		print("status: ", status)
		return bestMove.getPosition()
 
	# Traverse all cells, evaluate minimax function for
	# all empty cells. And return the cell with optimal
	# value.
	for i in range(board.MAX_WIDTH) :    
		for j in range(board.MAX_HEIGHT):
		 
			# Check if cell is empty
			#if (board[i][j] == '_') :
			if(board.isEmpty(i, j)):
			 
				# Make the move
				#board[i][j] = player
				board.setX(i, j)
 
				# compute evaluation function for this
				# move.
				var moveVal = minimax(board, 0, false)
 
				# Undo the move
				#board[i][j] = '_'
				board.setEmpty(i, j)
 
				# If the value of the current move is
				# more than the best value, then update
				# best/
				if (moveVal > bestVal) :               
					#bestMove = (i, j)
					bestMove.setPosition(i, j)
					bestVal = moveVal
 
	#print("The value of the best Move is :", bestVal)
	#print()
	return bestMove.getPosition()
