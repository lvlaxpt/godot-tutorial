extends Node

class_name Board

const MAX_WIDTH: int = 3
const MAX_HEIGHT: int = 3

#type
const CROSS = "X"
const CIRCLE = "O"
const EMPTY = "-"

const CROSS_WIN = 10
const CIRCLE_WIN = -10
const DRAW = 0


var board = null


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
# constructor
func _init():
	_initTileGrid()

func getType(x: int, y: int) -> String:
	return board[x][y]
	
	
func setO(x: int, y: int):
	_setType(x, y, CIRCLE)


func setX(x: int, y: int):
	_setType(x, y, CROSS)
	
func setEmpty(x: int, y: int):
	_setType(x, y, EMPTY)
	
func _setType(x: int, y: int, type):
	board[x][y] = type
	
	
func print():
	var data: String = ""
	for y in range(MAX_HEIGHT):
		for x in range(MAX_WIDTH):
			data = data + board[x][y]
		data = data + "\n"
	
	print(data)
			

func _initTileGrid():
	board = []
	board.resize(MAX_WIDTH)
	for x in range(MAX_WIDTH):
		board[x] = []
		board[x].resize(MAX_HEIGHT)
		for y in range(MAX_HEIGHT):
			board[x][y] = EMPTY


func hasMoreMoves() -> bool:
	for x in range(MAX_WIDTH) :
		for y in range(MAX_HEIGHT) :
			if (board[x][y] == EMPTY) :
				return true
	return false

func isEmpty(x: int, y: int) -> bool:
	return getType(x, y) == EMPTY

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
