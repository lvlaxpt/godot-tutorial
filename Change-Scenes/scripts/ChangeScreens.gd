extends Panel


# @see: https://www.youtube.com/watch?v=kzHeu7xaxoA 
var sceneOne: String = "res://scenes/Panel1.tscn"
var sceneTwo: String = "res://scenes/Panel2.tscn"
var scenePath: String = ''
var isTestingChangeSceneTo = false

# Our PackedScene Instance Reference
var sceneOneInstance: PackedScene
var sceneTwoInstance:PackedScene

var timerCounter: float = 0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	sceneTwoInstance = load("res://scenes/Panel2.tscn")
	sceneOneInstance = load("res://scenes/Panel1.tscn")
	scenePath = get_tree().get_current_scene().filename
	#print("scene ", sceneTwoInstance)
	#print("curr scene ", scenePath)


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func _physics_process(delta: float) -> void:
	if timerCounter > 2.0:
		print("inside physics process")
		
		if(isTestingChangeSceneTo):
				if(scenePath == sceneOne):
					Global.sceneNameTimer = "from scene one. Random: " + str(randi()%10+1)
					var message = get_tree().change_scene_to(sceneTwoInstance)
					print(message) # OK: 0
				if scenePath == sceneTwo:
					Global.sceneNameTimer = "from scene two. Random: " + str(randi()%10+1)
					var message = get_tree().change_scene_to(sceneOneInstance)
					print(message) # OK: 0
				
				
		else:
			if(scenePath == sceneOne):
				Global.sceneNameTimer = "from scene one. Random: " +str(randi()%10+1)
				var message = get_tree().change_scene(sceneTwo)
				print(message) # OK: 0
			if scenePath == sceneTwo:
				Global.sceneNameTimer = "from scene two. Random: " + str(randi()%10+1)
				var message = get_tree().change_scene(sceneOne)
				print(message) # OK: 0
		
		
	timerCounter += delta
